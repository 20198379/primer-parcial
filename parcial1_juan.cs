﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace primer_parcial
{
    class Program
    {
        static void Main(string[] args)
        {

            int canretirar;
            int C1000, C500, C200, C100;
            C1000 = 0;
            C500 = 0;
            C200 = 0;
            C100 = 0;

            // Limites
            int limiteRetiro = 20000, limiteTransaccion = 10000;
            int rt = 0;

            Console.Write("Bienvenido al Cajero FDP Inversments. ");
            Console.WriteLine("Por favor, seleccione su banco en base al número de la lista.");
            Console.WriteLine("1- FDP Inversments \n" +
                "2- Banco BanReservas \n" +
                "3- Banco Popular \n" +
                "4- Salir");
            int banco = Convert.ToInt32(Console.ReadLine());
            if (banco == 1 || banco == 2 || banco == 3)
            {
                Console.WriteLine("Bienvenido, ¿Qué desea hacer? \n" +
                    "Seleccione en base los numeros de la lista. \n" +
                    "1- Retiro \n" +
                    "2- Transacción \n" +
                    "3- Salir");
                int opcion = Convert.ToInt32(Console.ReadLine());
                if (opcion == 1)
                {
                    Console.WriteLine($"Por favor, ingrese la cantidad a retirar. El limite a retirar es de {limiteRetiro}"); // Informe de Limite a Retirar.
                    int cantidadRetirar = Convert.ToInt32(Console.ReadLine());

                    if (cantidadRetirar <= limiteRetiro)
                    {
                        Console.WriteLine("La Cantidad introducida es: " + cantidadRetirar);
                        if ((cantidadRetirar >= 1000))
                        {
                            C1000 = (cantidadRetirar / 1000);
                            cantidadRetirar = cantidadRetirar - (C1000 * 1000);
                        }
                        if ((cantidadRetirar >= 500))
                        {

                            
                            if ((cantidadRetirar >= 1000))
                            {
                                C1000 = (cantidadRetirar / 1000);
                                cantidadRetirar = cantidadRetirar - (C1000 * 1000);
                            }
                            if ((cantidadRetirar >= 500))
                            {
                                C500 = (cantidadRetirar / 500);
                                cantidadRetirar = cantidadRetirar - (C500 * 500);
                            }
                            if ((cantidadRetirar >= 200))
                            {
                                C200 = (cantidadRetirar / 200);
                                cantidadRetirar = cantidadRetirar - (C200 * 200);
                            }
                            if ((cantidadRetirar >= 100))
                            {
                                C100 = (cantidadRetirar / 100);
                                cantidadRetirar = cantidadRetirar - (C100 * 100);
                            }
                            Console.WriteLine("Billetes de 1000: " + C1000);
                            Console.WriteLine("Billetes de 500 : " + C500);
                            Console.WriteLine("Billetes de 200 : " + C200);
                            Console.WriteLine("Billetes de 100 : " + C100);
                        }
                        else
                        {
                            Console.WriteLine("Ninguna de las opciones disponibles ha sido seleccionada, por favor seleccione una.");
                        }
                    }
                    else if (cantidadRetirar > limiteRetiro)
                    {
                        Console.WriteLine("El monto ha superado el limite de Retiro, por lo tanto no puede ser dispensado");
                    }
                }

                else if (opcion == 2)
                {
                    Console.WriteLine($"Por favor, ingrese la cantidad a transferir. El limite a transferir es de {limiteTransaccion}"); // Informe de Limite a Retirar.
                    int cantidadTransferir = Convert.ToInt32(Console.ReadLine());
                    if (cantidadTransferir <= limiteTransaccion)
                    {
                        Console.WriteLine($"La cantidad a Transferir es de {cantidadTransferir}");
                        int cuentaTransiccion;
                        do
                        {
                            Console.WriteLine("Ingrese el número de cuenta a transferir");
                            cuentaTransiccion = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine($"Verificando el número de cuenta: {cuentaTransiccion}...");
                            if (cuentaTransiccion > 999999999)
                            {
                                Console.WriteLine("El número de cuenta introducido es invalido");
                            }
                        }
                        while (cuentaTransiccion > 999999999);

                        if (cuentaTransiccion <= 999999999)
                        {
                            Console.WriteLine("Valido");
                            Console.WriteLine("¿Seguro qué desea realizar esta transacción? \n" +
                                "Si \n" +
                                "No");
                            string respuesta = Console.ReadLine();
                            if (respuesta == "Si" || respuesta == "si")
                            {
                                Console.WriteLine("La transicción ha sido exitosa");
                            }
                            else if (respuesta == "No" || respuesta == "no")
                            {
                                Console.WriteLine("Gracias por utilizar el Cajero FDP Inversments");
                            }
                        }
                    }
                }
                else if (opcion == 3)
                {
                    Console.WriteLine("Gracias por utilizar el Cajero FDP Inversments");
                }
            }
            Console.ReadKey();
        }
    }
}



    
